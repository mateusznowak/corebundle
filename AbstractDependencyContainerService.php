<?php

namespace CoreBundle;

use Symfony\Component\Yaml\Yaml;

abstract class AbstractDependencyContainerService
{
    /* @var array $container */
    protected $container;

    /* @var array $parameters */
    protected $parameters;

    public function __construct()
    {
        $this->container = array();
        $this->shareParameters();
        $this->shareContainers();
    }

    public function get($containerName)
    {
        if (!array_key_exists($containerName, $this->container)) {
            throw new \RuntimeException(sprintf('Container %s not found.', $containerName));
        }

        return $this->container[$containerName];
    }

    protected function shareParameters()
    {
        $this->parameters = Yaml::parse(file_get_contents('../app/Bootstrap/config.yml'));
    }

    protected function getParameter($key)
    {
        if (!array_key_exists($key, $this->parameters)) {
            return $key;
        }

        return $this->parameters[$key];
    }

    protected function shareContainer($containerName, $closure)
    {
        $this->container[$containerName] = $closure;
    }

    abstract protected function shareContainers();
}
