<?php

namespace CoreBundle;

use Closure;

class Bootstrap
{
    protected $requestUri;
    protected $responseClosure;
    protected $beforeClosure;

    public function __construct()
    {
        $this->requestUri = $_SERVER['REQUEST_URI'];
    }
    
    public function before(Closure $closure)
    {
        $this->beforeClosure = $closure;
    }

    public function get($uri, Closure $closure)
    {
        if (FALSE === $this->isPost() && preg_match('#^' . $uri . '$#', $this->requestUri, $parameters)) {
            $this->responseClosure = $closure(
                $parameters
            );
        }
    }

    public function post($uri, Closure $closure)
    {
        if (TRUE === $this->isPost() && preg_match('#^' . $uri . '$#', $this->requestUri, $parameters)) {
            $this->responseClosure = $closure(
                $parameters
            );
        }
    }

    public function run()
    {
        if (!$this->responseClosure) {
            throw new Exception404('No route found');
        }
        
        if ($this->beforeClosure != NULL) {
            $before = $this->beforeClosure;
            $before();
        }

        echo $this->responseClosure;
    }

    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
}
